package br.com.dblogic.ennynmoria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EnnynMoriaApplication {

	public static void main(String[] args) {
		SpringApplication.run(EnnynMoriaApplication.class, args);
	}

}